﻿using LightsOff.Core.Interfaces;
using LightsOff.Data.Components;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace LightsOff.IO
{
    public static class FileIO
    {
        public static void Save<TCell>(Board<TCell> board, string filePath = @"..\..\SavedGames") where TCell : IGameCell, new()
        {
            getValidPath(ref filePath);

            using (StreamWriter file = File.CreateText(filePath))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, board);
            }
        }
        public static async Task SaveAsync<TCell>(Board<TCell> board, string filePath = @"..\..\SavedGames") where TCell : IGameCell, new()
        {
            getValidPath(ref filePath);

            string serialisedObj = getSerialisedStringFromObject(board);
            byte[] encodedObj = Encoding.UTF8.GetBytes(serialisedObj);

            using (FileStream stream = new FileStream(
                filePath,
                FileMode.Append,
                FileAccess.Write,
                FileShare.None,
                bufferSize: 4096,
                useAsync: true))
            {
                await stream.WriteAsync(encodedObj, 0, encodedObj.Length);
            };
        }

        public static Board<TCell> Load<TCell>(string filePath) where TCell : IGameCell, new()
        {
            /* Validate stuff */
            getValidPath(ref filePath);
            if (!File.Exists(filePath))
                throw new IOException("File does not exist!");

            using (StreamReader file = File.OpenText(filePath))
            {
                JsonSerializer serializer = new JsonSerializer();
                return (Board<TCell>)serializer.Deserialize(file, typeof(Board<TCell>));
            }

           // return JsonConvert.DeserializeObject<Board<TCell>>(ReadAllText(filePath, Encoding.UTF8));
        }
        public static async Task<Board<TCell>> LoadAsync<TCell>(string filePath) where TCell : IGameCell, new()
        {
            /* Validate stuff */
            getValidPath(ref filePath);
            if (!File.Exists(filePath))
                throw new IOException("File does not exist!");

            return JsonConvert.DeserializeObject<Board<TCell>>(await Utilities.AsyncFileReading.ReadAllTextAsync(filePath));
        }

        private static void getValidPath(ref string filePath)
        {
            if (string.IsNullOrEmpty(filePath))
                throw new IOException("Unexpected Empty or NULL filePath");

            if (!Path.HasExtension(filePath))
                filePath += @"\lightsoffsave.json";
            else if (!(Path.GetExtension(filePath) == ".json"))
                filePath += @".json";

            /* Got here, all is good */
        }
        private static string getSerialisedStringFromObject<TCell>(Board<TCell> board) where TCell : IGameCell, new()
        {
            var serialisedObj = string.Empty;
            try
            {
                serialisedObj = JsonConvert.SerializeObject(board);
            }
            catch (IOException) { }

            return string.IsNullOrEmpty(serialisedObj) ? string.Empty : serialisedObj;
        }

    }

}
