﻿using LightsOff.Core.Data;
using LightsOff.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Media;

namespace LightsOff.GUI
{
    public class Configuration : INotifyPropertyChanged
    {
        private Dictionary<State, Brush> Colors { get; }

        private static readonly Brush DefaultOffCellsBrush = Brushes.Black;
        private static readonly Brush DefaultOnCellsBrush = Brushes.LightYellow;
        private static readonly int DuplicationMark = 1;

        private static readonly object padLock = new object();
       

        private static Configuration _instance;
        public static Configuration Instance
        {
            get
            {
                lock(padLock)
                {
                    if (_instance == null)
                        _instance = new Configuration();
                    return _instance;
                }
            }

            private set
            {
                _instance = value;
            }
        }

        public Dictionary<State, Brush>.ValueCollection GameColors
        {
            get => Colors.Values;
        }

        public bool HasDuplicateColors
        {
            get => GameColors.GroupBy(a => a).Any(c => c.Count() > DuplicationMark);
        }

        public void RestoreInitialColors()
        {
            Colors[State.On] = DefaultOnCellsBrush;
            Colors[State.Off] = DefaultOffCellsBrush;
        }

        public Brush this[State state]
        {
            get => Colors[state];
            set
            {
                Colors[state] = value;
                OnPropertyChanged(nameof(Colors));
            }
        }

        private Configuration()
        {
            Colors = new Dictionary<State, Brush>
            {
                [State.Off] = DefaultOffCellsBrush,
                [State.On] = DefaultOnCellsBrush
            };
        }
        
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
