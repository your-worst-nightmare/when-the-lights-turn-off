﻿using LightsOff.Core.Data;
using LightsOff.GUI.Components;
using System.Collections.Generic;

namespace LightsOff.GUI.ViewModels
{
    public class SettingsWindowViewModel
    {
        public BoardView Elements { get; }

        private static readonly int DefaultSize = 7;
        public SettingsWindowViewModel()
        {
            Elements = BoardView.GetPreviewBoard(DefaultSize);
        }
    }
}
