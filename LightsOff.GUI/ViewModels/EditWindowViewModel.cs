﻿using LightsOff.Core.Data;
using LightsOff.GUI.Components;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightsOff.GUI.ViewModels
{
    public class EditWindowViewModel : INotifyPropertyChanged
    {
        private BoardView _elements;
        public BoardView Elements
        {
            get => _elements;
            set
            {
                _elements = value;
                OnPropertyChanged(nameof(Elements));
            }
        }

        private static readonly int StartingBoardSize = 5;
        public EditWindowViewModel()
        {
            Elements = BoardView.GetUniformBoard(StartingBoardSize, State.Off);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
