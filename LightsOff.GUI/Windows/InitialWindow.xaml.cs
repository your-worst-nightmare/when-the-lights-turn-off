﻿using LightsOff.GUI.ViewModels;
using System.Windows;

namespace LightsOff.GUI.Windows
{
    public partial class InitialWindow 
    {
        public InitialWindow()
        {
            InitializeComponent();           
        }

        private void MapEditor_ButtonClicked(object sender, RoutedEventArgs e)
        {
            var window = new EditorWindow();
            var viewModel = new EditWindowViewModel();
            window.DataContext = viewModel;
            window.ShowDialog();
        }

        private void Play_ButtonClicked(object sender, RoutedEventArgs e)
        {

        }

        private void Settings_ButtonClicked(object sender, RoutedEventArgs e)
        {
            var window = new SettingsWindow();
            var viewModel = new SettingsWindowViewModel();
            window.DataContext = viewModel;
            window.ShowDialog();
        }
    }
}
