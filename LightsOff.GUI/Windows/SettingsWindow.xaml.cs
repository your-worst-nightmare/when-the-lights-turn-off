﻿using LightsOff.Core.Data;
using System.Windows;
using System.Windows.Media;
using System.ComponentModel;

namespace LightsOff.GUI.Windows
{
    public partial class SettingsWindow : INotifyPropertyChanged
    {
        private static readonly BrushConverter Converter;

        private bool _hasDuplicateColors = false;
        public bool HasDuplicateColors
        {
            get => _hasDuplicateColors;
            private set
            {
                _hasDuplicateColors = value;
                OnPropertyChanged(nameof(HasDuplicateColors));
            }
        }

        static SettingsWindow()
        {
            Converter = new BrushConverter();
        }

        public SettingsWindow()
        {
            InitializeComponent();
            Closing += SettingsWindow_Closing;
        }

        private void SettingsWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Configuration.Instance.HasDuplicateColors)
                Configuration.Instance.RestoreInitialColors();
        }

        private void BoardButton_Clicked(object sender, RoutedEventArgs e)
        {

        }
        
        private static SolidColorBrush StringToBrush(string text)
        {
            return Converter.ConvertFromString(text) as SolidColorBrush;
        }

        private void OnCellsColorPicker_SelectionChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            Configuration.Instance[State.On] = StringToBrush(OnCellsColorPicker.SelectedColorText);
            HasDuplicateColors = Configuration.Instance.HasDuplicateColors;
        }

        private void OffCellsColorPicker_SelectionChanged(object sender, RoutedPropertyChangedEventArgs<Color?> e)
        {
            Configuration.Instance[State.Off] = StringToBrush(OffCellsColorPicker.SelectedColorText);
            HasDuplicateColors = Configuration.Instance.HasDuplicateColors;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
