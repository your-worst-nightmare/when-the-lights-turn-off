﻿using LightsOff.GUI.Components;
using LightsOff.GUI.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LightsOff.GUI.Windows
{
    public partial class EditorWindow
    {
        public EditorWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(DataContext is EditWindowViewModel viewModel)
            {
                viewModel.Elements = BoardView.GetUniformBoard(3, Core.Data.State.Off);
            }
        }
    }
}
