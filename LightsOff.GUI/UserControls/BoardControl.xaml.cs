﻿using LightsOff.Data.Components;
using LightsOff.GUI.Components;
using System;
using System.Windows;
using System.Windows.Controls;

namespace LightsOff.GUI.UserControls
{
    public partial class BoardControl : UserControl
    {
        public event EventHandler ButtonClick;

        public BoardControl()
        {
            InitializeComponent();
        }

        public Board<ColoredCell> BoardItem
        {
            get { return (Board<ColoredCell>)GetValue(BoardItemProperty); }
            set { SetValue(BoardItemProperty, value); }
        }

        public static readonly DependencyProperty BoardItemProperty = DependencyProperty.Register(
            "BoardItem", 
            typeof(Board<ColoredCell>), 
            typeof(BoardControl), 
            new PropertyMetadata(default(Board<ColoredCell>))
        );

        private void BoardButton_Clicked(object sender, RoutedEventArgs e) => ButtonClick?.Invoke(this, e);           
    }
}
