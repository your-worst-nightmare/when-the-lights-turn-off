﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace LightsOff.GUI.Converters
{
    class DuplicateColorsToTextVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool hasDuplicateColors)
                return hasDuplicateColors ? Visibility.Visible : Visibility.Hidden;
            return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Visibility visibility)
                return visibility == Visibility.Visible ? true : false;
            return false;
        }
    }
}
