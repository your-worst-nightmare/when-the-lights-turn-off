﻿using LightsOff.Core.Components;
using LightsOff.Core.Data;
using LightsOff.Utilities;
using System.Windows.Media;

namespace LightsOff.GUI.Components
{
    public class ColoredCell : Cell
    {
        public ColoredCell(Position position, State state) : 
            base(position, state)
        {
            Configuration.Instance.PropertyChanged += Instance_PropertyChanged;
        }

        public ColoredCell()
        {
            Configuration.Instance.PropertyChanged += Instance_PropertyChanged;
        }

        private void Instance_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(e.PropertyName == "Colors")
            {
                Color = Configuration.Instance[State];
            }
        }

        private Brush _color;
        public Brush Color 
        {
            get => Configuration.Instance[State];
            private set
            {
                _color = value;
                OnPropertyChanged(nameof(Color));
            }
        }
    }
}
