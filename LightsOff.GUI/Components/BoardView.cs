﻿using LightsOff.Core.Components;
using LightsOff.Core.Data;
using LightsOff.Data.Components;
using System.Collections.Generic;
using System.Windows.Media;

namespace LightsOff.GUI.Components
{
    public class BoardView
    {
        public Board<ColoredCell> Board { get; }
        
        public BoardView(int size, List<List<State>> states)
        {
            Board = new Board<ColoredCell>(size, states);
        }

        public static BoardView GetUniformBoard(int size, State overallState)
        {
            var states = new List<List<State>>(size);
            for (var row = 0; row < size; ++row)
            {
                states.Add(new List<State>(size));
                for (var column = 0; column < size; ++column)
                {
                    states[row].Add(overallState);
                }
            }
            return new BoardView(size, states);
        }

        public static BoardView GetPreviewBoard(int size)
        {
            var states = new List<List<State>>(size);
            for (var row = 0; row < size; ++row)
            {
                states.Add(new List<State>(size));
                for (var column = 0; column < size; ++column)
                {
                    states[row].Add((row + column) % 2 == 0 ? State.Off : State.On);
                }
            }
            return new BoardView(size, states);
        }
    }
}
