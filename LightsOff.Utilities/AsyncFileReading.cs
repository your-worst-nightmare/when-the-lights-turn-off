﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightsOff.Utilities
{
    public static class AsyncFileReading
    {
        private static readonly int DefaultBufferSize = 4096;
        private static readonly FileOptions DefaultOptions = FileOptions.Asynchronous | FileOptions.SequentialScan;
        public static Task<string> ReadAllTextAsync(string path)
        {
            return Task.FromResult(String.Join(" ", ReadAllLinesAsync(path, Encoding.UTF8)));
        }

        private static async Task<string[]> ReadAllLinesAsync(string path, Encoding encoding)
        {
            var lines = new List<string>();

            // Open the FileStream with the same FileMode, FileAccess
            // and FileShare as a call to File.OpenText would've done.
            using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read, DefaultBufferSize, DefaultOptions))
            using (var reader = new StreamReader(stream, encoding))
            {
                string line;
                while ((line = await reader.ReadLineAsync()) != null)
                {
                    lines.Add(line);
                }
            }

            return lines.ToArray();
        }


    }
}
