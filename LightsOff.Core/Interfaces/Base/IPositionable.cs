﻿namespace LightsOff.Core.Interfaces.Base
{
    using Utilities;

    public interface IPositionable
    {
        Position Position { get; set; }
    }
}
