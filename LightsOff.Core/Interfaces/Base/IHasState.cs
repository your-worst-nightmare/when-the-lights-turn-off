﻿namespace LightsOff.Core.Interfaces.Base
{
    public interface IHasState<StateType>
    {
        StateType State { get; set; }
    }
}
