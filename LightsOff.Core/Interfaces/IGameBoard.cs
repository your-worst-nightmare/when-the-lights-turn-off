﻿using System.Collections.Generic;

namespace LightsOff.Core.Interfaces
{
    public interface IGameBoard
    {
        ICollection<ICollection<IGameCell>> Cells { get; set; }
    }
}
