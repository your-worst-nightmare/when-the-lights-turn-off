﻿using LightsOff.Core.Data;
using LightsOff.Core.Interfaces.Base;
using System.ComponentModel;

namespace LightsOff.Core.Interfaces
{
    public interface IGameCell :  IHasState<State>, IPositionable, INotifyPropertyChanged
    {
    }
}
