﻿namespace LightsOff.Core.Data
{
    public enum State : byte
    {
        On,
        Off
    }
}
