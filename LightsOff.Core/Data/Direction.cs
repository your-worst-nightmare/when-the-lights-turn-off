﻿namespace LightsOff.Core.Data
{
    public enum Direction : byte
    {
        Up,
        Down,
        Left,
        Right
    }
}
