﻿using System.ComponentModel;
using LightsOff.Utilities;
using LightsOff.Core.Data;
using LightsOff.Core.Interfaces;

namespace LightsOff.Core.Components
{
    public class Cell : IGameCell
    {
        public Position Position { get; set; }

        private State _state;
        public State State
        {
            get => _state;
            set
            {
                _state = value;
                OnPropertyChanged(nameof(State));
            }
        }

        public Cell() { }

        public Cell(Position position, State state)
        {
            Position = position;
            State = state;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
