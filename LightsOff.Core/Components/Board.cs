﻿using LightsOff.Core.Interfaces;
using System.Collections.Generic;
using System.ComponentModel;
using System;
using LightsOff.Utilities;
using System.Linq;
using LightsOff.Core.Data;
using LightsOff.Core.Components;

namespace LightsOff.Data.Components
{
    public class Board<TCell> : INotifyPropertyChanged where TCell : IGameCell, new()
    {
        private static readonly Dictionary<Direction, Position> DirectionsToOffsets = new Dictionary<Direction, Position>(4)
        {
            {Direction.Up, new Position(-1, 0) },
            {Direction.Right, new Position(0, 1) },
            {Direction.Down, new Position(1, 0) },
            {Direction.Left, new Position(0, -1) },
        };
      
        private static readonly int ZeroIndex = 0;

        private List<List<TCell>> _cells;
        public List<List<TCell>> Cells
        {
            get => _cells;
            set
            {
                _cells = value;
                OnPropertyChanged(nameof(Cells));
            }
        }

        public int CellsOffCount { get; private set; }

        public void PressCell(Position position)
        {
            PressCell(position.Row, position.Column);
        }

        public void PressCell(int row, int column)
        {
            Func<int, int, bool> areOutOfBounds = (r, c) =>
            ((r < ZeroIndex || r >= Cells.Count) || (c < ZeroIndex || c >= Cells.Count));

            if(areOutOfBounds(row, column))
                throw new IndexOutOfRangeException("Row or column out of range.");

            var state = Cells[row][column].State;
            Cells[row][column].State = (state == State.On) ? State.Off : State.On;

            foreach (var direction in DirectionsToOffsets.Keys)
            {
                var rowCopy = row;
                var columnCopy = column;
                var offsets = DirectionsToOffsets[direction];

                rowCopy += offsets.Row;
                columnCopy += offsets.Column;
                if (areOutOfBounds(rowCopy, columnCopy))
                    continue;

                state = Cells[rowCopy][columnCopy].State;
                Cells[rowCopy][columnCopy].State = (state == State.On) ? State.Off : State.On;
            }
        }

        public Board(int size, List<List<State>> states)
        {
            Cells = new List<List<TCell>>(size);
            for(int row = 0; row < size; ++row)
            {
                Cells.Add(new List<TCell>(size));
                for(int column = 0; column < size; ++column)
                {
                    //TODO: Clean up the cell creation code
                    var cell = new TCell
                    {
                        Position = new Position(row, column),
                        State = states[row][column]
                    };

                    cell.PropertyChanged += Cell_PropertyChanged;
                    Cells[row].Add(cell);
                }
            }
        }

        private void Cell_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName == nameof(Cell.State))
            {
                CellsOffCount = Cells.SelectMany(l => l).Count(c => c.State == State.Off);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
